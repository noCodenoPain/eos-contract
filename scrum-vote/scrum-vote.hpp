#include <eosiolib/eosio.hpp>
#include <string>

namespace scrummaster {
  //@abi voteEvent
  struct voteEvent {
    account_name creator;
    account_name to;
    uint64_t score;
    string comment;
    uint64_t createTime;

    uint64_t getScore() const { return score; }
    string getComment() const { return comment; }
    // call marco
    EOSLIB_SERIALIZE(voteEvent, (creator)(to)(score)(comment)(createTime))
  };

  //@abi member 
  struct member {
    account_name account;
    uint64_t scoreUsedVote;
    uint64_t scoreLeftToVote;
    uint64_t scoreForVote;

    uint64_t get_creator() const { return account; }
    EOSLIB_SERIALIZE(member, (account)(scoreForVote)(scoreLeftToVote)(scoreUsedVote))
  };
}
