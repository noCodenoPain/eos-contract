#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/multi_index.hpp>
#include <eosiolib/asset.hpp>

using namespace eosio;
using namespace std;

using std::string;
using std::vector;

#include "scrum-vote.hpp"

using namespace scrummaster;

class scrum_vote : public eosio::contract {

public:  
  scrum_vote(account_name self) 
  :eosio::contract(self),
  votes(_self, _self) 
  {}

  //@abi action
  void create(const account_name creator, vector<account_name> members,
              const uint64_t scoreForVote, const string &title,
              const string &content, const uint64_t createTime, const uint64_t endTime) {
    vector<member> _members;
    member m;

    for(int i = 0; i < members.size(); i++) {
      account_name name = members[i];
      m.account = name;
      m.scoreForVote = scoreForVote;
      _members.push_back(m);
    }

    vector<voteEvent> _votes;

    votes.emplace(creator, [&](auto &v) {
      v.pkey = votes.available_primary_key();
      v.creator = creator;
      v.title = title;
      v.content = content;
      v.members = _members;
      v.voteEvents = _votes;
      v.endTime = endTime;
      v.createTime = createTime;
    });
  }

  //@abi action
  void run(const uint64_t pkey, const account_name creator, vector<voteEvent> events)
  {
    // do not require_auth since want to allow anyone to call
    // verify already exist
    auto vote = votes.find(pkey);
    eosio_assert(vote != votes.end(), "Vote for pkey not found");

    vector<member> _members;
    auto members = vote->members;
    member m;
    uint64_t _scoreWannaVote;

    for(int i = 0; i < events.size(); i++) {
      voteEvent v = events[i];
      if (v.creator == creator) {
        _scoreWannaVote += v.score;
      } else {
        eosio_assert(false, "The creator and voter must be the same persion");
        return;
      }

      if(v.creator == v.to) {
        eosio_assert(false, "Can not vote for yourself");
        return;
      }
    }

    for(int i = 0; i < members.size(); i++) {
      member _account = members[i];

      if (creator == _account.account) {
        if (_account.scoreForVote < _account.scoreUsedVote + _scoreWannaVote) {
            eosio_assert(false, "This guy don't have enough score to vote");
            return;
        } else {
            m.scoreUsedVote = _account.scoreUsedVote + _scoreWannaVote;
            m.scoreLeftToVote = m.scoreForVote - m.scoreUsedVote;
        }
      } else {
            m.scoreUsedVote = _account.scoreUsedVote;
            m.scoreLeftToVote = _account.scoreLeftToVote;
      }

      m.account = _account.account;
      m.scoreForVote = _account.scoreForVote;
      _members.push_back(m);
    }

    //check if the person already vote
    votes.modify(vote, 0, [&](auto &v) {
      v.members = _members;
      auto currentEvents = v.voteEvents;
      for(int i = 0; i < events.size(); i++) {
        v.voteEvents.push_back(events[i]);
      }
    });
  }

private:
  // mark with @abi table so that eosiocpp will add this as a multi_index to the ABI with an index of type i64

  // @abi table votes i64
    struct vote {
      uint64_t pkey;
      account_name  creator;
      std::string title;
      std::string content;
      vector<member> members;
      vector<voteEvent> voteEvents;
      uint64_t endTime;
      uint64_t createTime;

      auto primary_key() const { return pkey; }
      auto get_author() const { return creator; }

      EOSLIB_SERIALIZE(vote, (pkey)(creator)(title)(content)(members)(voteEvents)(createTime)(endTime))
    };

    typedef eosio::multi_index<N(votes), vote> vote_table;
    vote_table votes;

};

EOSIO_ABI(scrum_vote, (run)(create))
